//DONE- "We need to add an event listener for when the DOM loads."
//DONE- "Let's declare a variable that will hold the URL for the API that we just created."
//DONE- "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."

// My Pseudocode:
// Done - code need to create:
// Done - when page loads, call the api_list_states 

// Done - GET response pulls the data to use from the database

// loop through the data
//for each state in data
//create an option element that has the value of the abbreviation and the text of the name

window.addEventListener('DOMContentLoaded', async() => {

    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            alertTrigger.addEventListener('click', function() {
                alert('You have a bad response')
            })

        } else {
            const data = await response.json();

            const selectTag = document.getElementById('state');
            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener("submit", async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));

                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                }
            })
        }

    } catch (e) {
        console.error(e)
    }

});



// all the data from the response is going to be used and set
//  to data

// Their code without the Try/Catch blocks:

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/states/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       console.log(data);
//     }

//   });