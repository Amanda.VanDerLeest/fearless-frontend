window.addEventListener('DOMContentLoaded', async() => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        // if conference information is done loading, hide spinner?
        const spinnerTag = document.querySelector('.spinner-grow')
        spinnerTag.classList.add("d-none");

        // if conference information is done loading, remove the d-none hider
        // this made the "choose a conference" dropdown appear
        selectTag.classList.remove('d-none');

        // this starts the form section
        const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener("submit", async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);
            // used backticks because this one will have different varibales
            const locationUrl = `http://localhost:8001${formData.get('conference')}attendees/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {

                // remove the d-none from the success alert
                const successAlertTag = document.getElementById('success-message');
                console.log("alert:", successAlertTag);
                successAlertTag.classList.remove("d-none");


                // add d-none to the form
                const formTag = document.getElementById('create-attendee-form');
                formTag.classList.add('d-none');

                formTag.reset();
                const newAttendee = await response.json();
            }
        })

    }

});