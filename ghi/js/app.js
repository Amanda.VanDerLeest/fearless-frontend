function getFormattedDate(date) {
    return new Intl.DateTimeFormat('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' }).format(new Date(date))
}

function createCard(name, location, description, pictureUrl, start, end) {
    return `
    <div class="col col-lg-4">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted"> ${start} - ${end}</div>
      </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async() => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            alertTrigger.addEventListener('click', function() {
                    alert('You have a bad response')
                })
                // Figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const location = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = getFormattedDate(details.conference.starts);
                    const end = getFormattedDate(details.conference.ends);
                    const html = createCard(name, location, description, pictureUrl, start, end);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }

        }
    } catch (e) {
        console.error(e)
    }

});